import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComputerService, NetworkService, UserService } from './services';
import { NetworkComponent } from './components/network/network.component';
import { ComputerComponent } from './components/computer/computer.component';
import { UserComponent } from './components/user/user.component';

@NgModule({
  declarations: [AppComponent, NetworkComponent, ComputerComponent, UserComponent],
  imports: [BrowserModule, HttpClientModule, AppRoutingModule],
  providers: [UserService, NetworkService, ComputerService],
  bootstrap: [AppComponent],
})
export class AppModule {}
