import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Computer } from 'src/app/models/computer';
import { ComputerService } from 'src/app/services';

@Component({
  selector: 'app-computer',
  templateUrl: './computer.component.html',
  styleUrls: ['./computer.component.css'],
})
export class ComputerComponent implements OnInit {
  selectedComputer!:Computer;
  computers: string = '';

  constructor(private computerService: ComputerService) {}

  ngOnInit(): void {}

  showComputers() {
    this.computerService.getComputers().subscribe((computers: Computer[]) => {
      this.computers = JSON.stringify(computers);
    });
  }

  addComputer(){
    this.selectedComputer = Object.assign({
      id:2000,
      name:"tester",
      speed:23
    });
    this.computerService.addComputer(this.selectedComputer).subscribe((c:Computer)=>{
      this.selectedComputer = c;
    })
  }

  updateComputer(){
    this.selectedComputer.speed = 10000;
    this.computerService.updateComputer(this.selectedComputer).subscribe((c:Computer)=>{
      this.selectedComputer = c;
    });
  }

  deleteComputer(){
    this.computerService.deleteComputer(this.selectedComputer).subscribe(()=>{
      alert("deleted!");
    });
  }

}
