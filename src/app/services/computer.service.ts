import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Computer } from '../models/computer';

@Injectable({
  providedIn: 'root'
})
export class ComputerService {

  readonly url:string = environment.URL + ':' + environment.PORT + '/computers';

  constructor(private http: HttpClient) { }

  // CREATE - POST
  addComputer(computer: Computer): Observable<Computer> {
    return this.http.post<Computer>(this.url,computer);
  }

   // READ - GET
  getComputers(){
    return this.http.get<Computer[]>(this.url);
  }

  // UPDATE - PUT
  updateComputer(computer: Computer): Observable<Computer> {
    return this.http.put<Computer>(this.url + "/" + computer.id, computer);
  }

  // DELETE - DELETE
  deleteComputer(computer: Computer) {
    return this.http.delete(this.url + "/" + computer.id);
  }
}
