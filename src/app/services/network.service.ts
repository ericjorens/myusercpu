import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Network } from '../models/network';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  readonly url:string = 'http://localhost:3000/networks';

  constructor(private http: HttpClient) { }

  getNetworks(){
    return this.http.get<Network[]>(this.url);
  }

}
